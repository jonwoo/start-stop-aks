# start-stop-runbook


## How to adhoc start/stop cluster

1. Start [pipeline](https://gitlab.thalesdigital.io/ifec-devsecops/cloud-infra/azure/terraform-products/devops-tools/start-stop-runbook/-/pipelines/new) with latest tag. 

2. Select the AKS cluster that you want to start/stop from the dropdown list. Default selection of "" will result in pipeline failure.
![Alt text](README-img/01-variables.png)
*If dropdown does not appear, please refresh the page. Or you can key it in manually (ensure spelling is correct).*

3. Run plan followed by apply for the required job and environment.
    - 02-start-aks
    - 03-stop-aks
