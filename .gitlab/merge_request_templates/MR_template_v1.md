### MR Checklist
* [ ] I included the team name in the title of the MR and description is meaningful
* [ ] I previewed the terraform plan of the pipeline
* [ ] I acknowledge that these changes will be applied only after MR will be approved
* [ ] I acknowledge that all resources created by IaC will not be edited in GUI

After merging into the master, you can view the progress of your changes in your pipeline.