# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)

### Added
### Changed
### Fixed

## [Unreleased]
## [Released]

## [1.2.1] 2023-03-02
### Changed
- Rename automation account name ends with account-vm
- [CCOE-898](https://thales-factory.atlassian.net/browse/CCOE-898)

## [1.2.0] 2023-02-23
### Added
- Added CCOE-898: Continue new runbook for start stop vm + lab-multisite-vm stop during weekend
- [CCOE-898](https://thales-factory.atlassian.net/browse/CCOE-898)

## [1.1.0] 2023-02-08
### Added
- Added Adhoc Start && Adhoc Stop jobs for AKS clusters
- [CCOE-856](https://thales-factory.atlassian.net/browse/CCOE-856)

## [1.0.8] 2023-01-04
- Store tfstate on one file instead of two blobs

## [1.0.7] 2023-01-04
- Remove duplicated schedules

## [1.0.6] 2023-01-04
- Singapore timezone update

## [1.0.5] 2022-11-22
- Changed to new runners

## [1.0.4] 2022-11-10
- Added dependency fix to unlink schedules

## [1.0.3] 2022-10-24
- Added TDF and IFEC AKS clusters

## [1.0.2] 2022-10-10
- Changed ClusterPrefix to clusterprefix

## [1.0.1] 2022-10-03
- Fix vars names

## [1.0.0] 2022-09-26
- Runbooks: deploy runbooks on new subscriptions.
- [CCOE-264](https://thales-factory.atlassian.net/browse/CCOE-264-refactor)
