### 01.Create Common Locals
locals {
  dirs      = split("/", path_relative_to_include())
  env       = local.dirs[1]
  product   = local.dirs[2]
  env_short = substr(local.env, 0, 1)
  tenant    = local.dirs[0]

  common = yamldecode(file(("common_vars.yaml")))

  project             = "ifec"
  location            = "East US 2"
  location_name       = "uset2"
  location_name_short = "ue2"
  product_group       = "ptf"
  storage_id          = "02"
  env_id              = "01"
  group               = "pdg"
  tenant_identifier   = split("-", local.dirs[0])[0]

  file_key = join("/", [
    local.project,
    local.env,
    local.product,
    "terraform.tfstate"
  ])

  resource_group_name = join("-", [
    "ife",
    local.group,
    local.env,
    local.location_name,
    local.product_group,
    local.storage_id,
    "tfstate-rg"
  ])

  storage_account_name = join("", [
    "ife",
    local.group,
    local.env_short,
    local.location_name_short,
    local.product_group,
    local.storage_id,
    "tfssa"
  ])

  container_name = "tfstate"

}

### 02.Create Remote State where state file will be stored
remote_state {
  backend = "azurerm"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
  config = {
    key                  = local.file_key
    resource_group_name  = local.resource_group_name
    storage_account_name = local.storage_account_name
    container_name       = local.container_name
  }
}

### 03.Create Inputs
inputs = merge(
  {
    storage_account_name = local.storage_account_name
    project              = local.project
    env                  = local.env
    env_id               = local.env_id
    env_short            = local.env_short
    group                = local.group
    product_group        = "sre"
    location             = local.location
    location_name        = local.location_name
    location_name_short  = local.location_name_short
    base_tags = merge(
      local.common.base_tags,
      {
        Environment = local.env
    })
})
