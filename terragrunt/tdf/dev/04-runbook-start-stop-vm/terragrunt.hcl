include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform/products//04-runbook-start-stop-vm"
}

dependency "runbook" {
  config_path = "../01-runbook-start-stop"
}

inputs = {
  resource_group_name = dependency.runbook.outputs.resource_group_name
  location           = "East US 2"

  schedules = {
    lab-multisite = {
      vm_prefix      = "lab-multisite-"
      timezone       = "America/Los_Angeles"
      start_time     = "T08:00:00-05:00"
      stop_time      = "T08:00:00-05:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    qa-e2e = {
      vm_prefix      = "qa-e2e-"
      timezone       = "America/Los_Angeles"
      start_time     = "T08:00:00-05:00"
      stop_time      = "T08:00:00-05:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    test = {
      vm_prefix      = "test-"
      timezone       = "America/Los_Angeles"
      start_time     = "T08:00:00-08:00"
      stop_time      = "T11:00:00-08:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    tooling = {
      vm_prefix      = "tooling-"
      timezone       = "America/Los_Angeles"
      start_time     = "T08:00:00-08:00"
      stop_time      = "T11:00:00-08:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },

  }
}
