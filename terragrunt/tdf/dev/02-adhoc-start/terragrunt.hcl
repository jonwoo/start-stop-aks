include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform/products//02-adhoc-start"
}

dependency "runbook" {
  config_path = "../01-runbook-start-stop"
}

inputs = {
  cluster                 = get_env("CLUSTER", "")
  resource_group_name     = dependency.runbook.outputs.resource_group_name
  automation_account_name = dependency.runbook.outputs.automation_account_name
  runbook_name            = dependency.runbook.outputs.runbook_name
}