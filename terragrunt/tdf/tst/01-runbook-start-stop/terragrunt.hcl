include {
  path = find_in_parent_folders()
}

terraform {
  source = "${get_parent_terragrunt_dir()}/../terraform/products//01-runbook-start-stop"
}

inputs = {
  location = "East US 2"

  schedules = {
    flytedge = {
      cluster_prefix = "flytedge-"
      timezone       = "Asia/Singapore"
      start_time     = "T08:00:00+08:00"
      stop_time      = "T08:00:00+08:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    cms = {
      cluster_prefix = "cms-"
      timezone       = "Asia/Kolkata"
      start_time     = "T08:00:00+05:30"
      stop_time      = "T08:00:00+05:30"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    gcms = {
      cluster_prefix = "gcms-"
      timezone       = "Asia/Singapore"
      start_time     = "T08:00:00+08:00"
      stop_time      = "T08:00:00+08:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    inflytanalytics = {
      cluster_prefix = "ifa-"
      timezone       = "Asia/Singapore"
      start_time     = "T08:00:00+08:30"
      stop_time      = "T08:00:00+05:30"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    ife-pdg-clusters = {
      cluster_prefix = "ife-pdg-"
      timezone       = "Asia/Kolkata"
      start_time     = "T08:00:00+05:30"
      stop_time      = "T08:00:00+05:30"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },
    platform = {
      cluster_prefix = "platform-"
      timezone       = "Asia/Singapore"
      start_time     = "T08:00:00-08:00"
      stop_time      = "T11:00:00-08:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    },

  }
}