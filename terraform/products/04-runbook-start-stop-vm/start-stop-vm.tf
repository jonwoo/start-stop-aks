locals {
  name_prefix = join("-", [
    "ife",
    "pdg",
    var.env,
    "sre",
    "automation"
  ])
  schedules_list = [for k, v in var.schedules : k]
}

data "azurerm_subscription" "this" {}

resource "azurerm_role_assignment" "this" {
  scope                = data.azurerm_subscription.this.id
  role_definition_name = "Contributor"
  principal_id         = azurerm_automation_account.automation_account.identity[0].principal_id
}

## Create Automation Account
resource "azurerm_automation_account" "automation_account" {
  name                = join("-", [local.name_prefix, "account-vm"])
  location            = var.location
  resource_group_name = var.resource_group_name
  sku_name            = "Basic"
    identity {
    type = "SystemAssigned"
  }
  tags                = local.tags
}

## Create Powershell Runbook
resource "azurerm_automation_runbook" "automation_runbook" {
  name                    = join("-", [local.name_prefix, "stop-start-vm-runbook"])
  location                = var.location
  resource_group_name     = var.resource_group_name
  automation_account_name = azurerm_automation_account.automation_account.name
  log_verbose             = "true"
  log_progress            = "true"
  runbook_type            = "PowerShell"
  description             = "Automation Runbook to Start/Stop VMs"
  tags                    = local.tags
  content                 = file("src/startstop-vm.ps1")
}

## Create Stop Schedules
resource "azurerm_automation_schedule" "auto_stop" {
  for_each                = toset(local.schedules_list)
  name                    = join("", [local.name_prefix, each.key, "stop-schedule"])
  resource_group_name     = var.resource_group_name
  automation_account_name = azurerm_automation_account.automation_account.name
  frequency               = "Week"
  interval                = "1"
  timezone                = var.schedules[each.key]["timezone"]
  description             = "Stop ${var.schedules[each.key]["vm_prefix"]} VM"
  start_time              = "${substr(timeadd(timestamp(), "24h"), 0, 10)}${var.schedules[each.key]["stop_time"]}"
  week_days               = [var.schedules[each.key]["stop_days"]]
}

## Create Stop Schedule link to Runbook for Common
resource "azurerm_automation_job_schedule" "stop_vm_schedule" {
  for_each                = toset(local.schedules_list)
  resource_group_name     = var.resource_group_name
  automation_account_name = azurerm_automation_account.automation_account.name
  runbook_name            = azurerm_automation_runbook.automation_runbook.name
  schedule_name           = join("", [local.name_prefix, each.key, "stop-schedule"])
  parameters = {
    action = "stop"
    vmprefix = var.schedules[each.key]["vm_prefix"]
  }
  depends_on = [
    azurerm_automation_account.automation_account, 
    azurerm_automation_runbook.automation_runbook, 
    azurerm_automation_schedule.auto_stop
  ]
}

## Create Start Schedule for Common
resource "azurerm_automation_schedule" "auto_start" {
  for_each                = toset(local.schedules_list)
  name                    = join("", [local.name_prefix, each.key, "start-schedule"])
  resource_group_name     = var.resource_group_name
  automation_account_name = azurerm_automation_account.automation_account.name
  frequency               = "Week"
  interval                = "1"
  timezone                = var.schedules[each.key]["timezone"]
  description             = "Start ${var.schedules[each.key]["vm_prefix"]} VM"
  start_time              = "${substr(timeadd(timestamp(), "24h"), 0, 10)}${var.schedules[each.key]["start_time"]}"
  week_days               = [var.schedules[each.key]["start_days"]]
}

## Create Start Schedule link to Runbook for Common
resource "azurerm_automation_job_schedule" "start_vm_schedule" {
  for_each                = toset(local.schedules_list)
  resource_group_name     = var.resource_group_name
  automation_account_name = azurerm_automation_account.automation_account.name
  runbook_name            = azurerm_automation_runbook.automation_runbook.name
  schedule_name           = join("", [local.name_prefix, each.key, "start-schedule"])
  parameters = {
    action = "start"
    vmprefix = var.schedules[each.key]["vm_prefix"]
  }
  depends_on = [
    azurerm_automation_account.automation_account, 
    azurerm_automation_runbook.automation_runbook, 
    azurerm_automation_schedule.auto_start
  ]
}

## Outputs
output "automation_account_name" {
  value       = azurerm_automation_account.automation_account.name
  description = "Automation Account Name"
}

output "runbook_name" {
  value       = azurerm_automation_runbook.automation_runbook.name
  description = "Runbook Name"
}
