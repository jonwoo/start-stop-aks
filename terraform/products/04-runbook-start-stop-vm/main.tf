## Providers
terraform {
  required_version = "~>1.2.5"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.40.0"
    }
  }
}

provider "azurerm" {
  features {}
}

## Data
data "azurerm_client_config" "current" {}

## Tags
locals {
  tags = merge(
    var.base_tags,
    {
      AppRole = "04-sre-runbook-start-stop-vm"
      EndDate = var.end_date
      Version = var.product_version
    }
  )
}

## Variables
variable "base_tags" {
  description = "base tags to apply"
  type = object({
    Contact             = string
    Created-By          = string
    Environment         = string
    Multi-Tenancy       = string
    Owner               = string
    Product             = string
    Source              = string
    Tier                = string
  })
}

variable "product_version" {
  description = "product version"
  type        = string
  default     = "0.0.0"
}

variable "env" {
  description = "Environment for which resources are created"
  type        = string
}

variable "end_date" {
  description = "null for all resources except which needs to be removed for garbage collection. Formatted as ISO-8601 date"
  type        = string
  default     = ""
}

variable "group" {
  description = "Name of the group. pdg or cit"
  type        = string
}

variable "location_name" {
  description = "location Name for resource naming"
  type        = string
}

variable "product_group" {
  description = "Product group"
  type        = string
  default     = "ptf"
}

variable "env_id" {
  description = "Environment Id"
  type        = string
}

variable "location" {
  description = "Location of the resource group in Azure"
  default     = "East US 2"
  type        = string
}

variable "log_verbose" {
  description = "Log versobe"
  type        = bool
  default     = true
}

variable "log_progress" {
  description = "Log progress"
  type        = bool
  default     = true
}

variable "timezone" {
  description = "Runbook schedule timezone"
  type        = string
  default     = "America/Los_Angeles"
}

variable "resource_group_name" {
  description = "Resource group name to be imported"
  type        = string
}

variable "schedules" {
  description = "Runbook schedule"
  default = {
    lab-multisite = {
      vm_prefix      = "lab-multisite-"
      timezone       = "America/Los_Angeles"
      start_time     = "T08:00:00-05:00"
      stop_time      = "T08:00:00-05:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    }
  }
  type = map(map(string))
}


