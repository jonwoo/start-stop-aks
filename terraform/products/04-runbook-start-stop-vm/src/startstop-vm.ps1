Param(   
    [Parameter(Mandatory = $true,
    ValueFromPipelineByPropertyName=$false, 
    HelpMessage="List your VMs")]
    [ValidateSet("Start","Stop")]
    [String]
    $Action,
    [Parameter(Mandatory=$True,
    ValueFromPipelineByPropertyName=$false,
    HelpMessage='VM prefix.')]
    [String]
    $VMPrefix
) 

# Ensures you do not inherit an AzContext in your runbook
Disable-AzContextAutosave -Scope Process | Out-Null

# Connect to Azure with system-assigned managed identity
$AzureContext = (Connect-AzAccount -Identity).context
Write-Output "$AzureContext"
(Connect-AzAccount -Identity).context

# Setting context to a specific subscription
$AzureContext = Set-AzContext -SubscriptionName $AzureContext.Subscription -DefaultProfile $AzureContext
Write-Output "$AzureContext"
Set-AzContext -SubscriptionName $AzureContext.Subscription -DefaultProfile $AzureContext

# Sign in to your Azure subscription
$sub = Get-AzSubscription -ErrorAction SilentlyContinue
if(-not($sub)) {
	Connect-AzAccount
}

if ($Action -eq "Start") {
    $VMs = get-azvm
    foreach ($VM in $VMs)
    {
        [Hashtable]$VMTag = (Get-AzVM -ResourceGroupName $VM.ResourceGroupName -Name  $VM.Name).Tags
        foreach ($h in $VMTag.GetEnumerator()) {
        if (($h.Name -eq "autostart") -and ($h.value -eq "1"))
            {
            If ($VM.Name.StartsWith("$VMPrefix"))
                {
                Start-AzVM -Name $VM.Name -ResourceGroupName $vm.ResourceGroupName -NoWait
                Write-Output "Starting VM with tags autostart:1 " $VM.Name
                }
            Else
				{
				Write-Output "This VM is not going to be started: " $VM.Name
				}
            }
        }
    }
}

if ($Action -eq "Stop") {
    $VMs = get-azvm
    foreach ($VM in $VMs)
    {
        [Hashtable]$VMTag = (Get-AzVM -ResourceGroupName $VM.ResourceGroupName -Name  $VM.Name).Tags
        foreach ($h in $VMTag.GetEnumerator()) {
        if (($h.Name -eq "autostop") -and ($h.value -eq "1"))
            {
            If ($VM.Name.StartsWith("$VMPrefix"))
                {
                    Stop-AzVM -Name $VM.Name -ResourceGroupName $vm.ResourceGroupName -NoWait -Force
                    Write-Output "Stopping VM with tags autostop:1 " $VM.Name
                }
            Else
                {
                    Write-Output "This VM is not going to be stopped: " $VM.Name
                }
            }
        }
    }
}

