<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.2.5 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | 3.40.0 |
| <a name="requirement_http"></a> [http](#requirement\_http) | 3.2.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 3.40.0 |
| <a name="provider_http"></a> [http](#provider\_http) | 3.2.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_automation_webhook.webhook_start](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/automation_webhook) | resource |
| [azurerm_client_config.current](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/data-sources/client_config) | data source |
| [http_http.runbook_webhook_call](https://registry.terraform.io/providers/hashicorp/http/3.2.1/docs/data-sources/http) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_automation_account_name"></a> [automation\_account\_name](#input\_automation\_account\_name) | Automation Account Name | `string` | n/a | yes |
| <a name="input_base_tags"></a> [base\_tags](#input\_base\_tags) | base tags to apply | <pre>object({<br>    Contact       = string<br>    Created-By    = string<br>    Environment   = string<br>    Multi-Tenancy = string<br>    Owner         = string<br>    Product       = string<br>    Source        = string<br>    Tier          = string<br>  })</pre> | n/a | yes |
| <a name="input_cluster"></a> [cluster](#input\_cluster) | AKS Cluster | `string` | n/a | yes |
| <a name="input_end_date"></a> [end\_date](#input\_end\_date) | null for all resources except which needs to be removed for garbage collection. Formatted as ISO-8601 date | `string` | `""` | no |
| <a name="input_env"></a> [env](#input\_env) | Environment for which resources are created | `string` | n/a | yes |
| <a name="input_env_id"></a> [env\_id](#input\_env\_id) | ID of environment | `string` | n/a | yes |
| <a name="input_group"></a> [group](#input\_group) | Name of the group. pdg or cit | `string` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | The resource group location in Azure | `string` | `"East US 2"` | no |
| <a name="input_location_name"></a> [location\_name](#input\_location\_name) | location Name for resource naming | `string` | n/a | yes |
| <a name="input_product_group"></a> [product\_group](#input\_product\_group) | Product group | `string` | `"ifa"` | no |
| <a name="input_product_version"></a> [product\_version](#input\_product\_version) | product version | `string` | `"0.0.0"` | no |
| <a name="input_resource_group_name"></a> [resource\_group\_name](#input\_resource\_group\_name) | Resource Group Name | `string` | n/a | yes |
| <a name="input_runbook_name"></a> [runbook\_name](#input\_runbook\_name) | Runbook Name | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cluster"></a> [cluster](#output\_cluster) | n/a |
| <a name="output_webhook_status_code"></a> [webhook\_status\_code](#output\_webhook\_status\_code) | n/a |
<!-- END_TF_DOCS -->