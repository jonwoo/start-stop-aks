resource "azurerm_automation_webhook" "webhook_start" {
  name                    = "adhoc-start"
  resource_group_name     = var.resource_group_name
  automation_account_name = var.automation_account_name
  runbook_name            = var.runbook_name
  expiry_time             = timeadd(timestamp(), "2m")
  parameters = {
    action        = "Start"
    clusterprefix = "${var.cluster}-"
  }
}

data "http" "runbook_webhook_call" {
  url          = sensitive(azurerm_automation_webhook.webhook_start.uri)
  method       = "POST"
  request_body = ""
}

output "webhook_status_code" {
  value = "Webhook call status code: ${data.http.runbook_webhook_call.status_code}"
}