## providers
terraform {
  required_version = "~>1.2.5"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.40.0"
    }
    http = {
      source  = "hashicorp/http"
      version = "3.2.1"
    }
  }
}

provider "azurerm" {
  features {}
}

provider "http" {
}

## data
data "azurerm_client_config" "current" {}

## tags
locals {
  tags = merge(
    var.base_tags,
    {
      Subproduct = "02-adhoc-start"
      EndDate    = var.end_date
      Version    = var.product_version
    }
  )
}

## variables
variable "base_tags" {
  description = "base tags to apply"
  type = object({
    Contact       = string
    Created-By    = string
    Environment   = string
    Multi-Tenancy = string
    Owner         = string
    Product       = string
    Source        = string
    Tier          = string
  })
}

variable "product_version" {
  description = "product version"
  type        = string
  default     = "0.0.0"
}

variable "env" {
  description = "Environment for which resources are created"
  type        = string
}

variable "end_date" {
  description = "null for all resources except which needs to be removed for garbage collection. Formatted as ISO-8601 date"
  type        = string
  default     = ""
}

variable "group" {
  description = "Name of the group. pdg or cit"
  type        = string
}

variable "location_name" {
  description = "location Name for resource naming"
  type        = string
}

variable "product_group" {
  description = "Product group"
  type        = string
  default     = "ifa"
}

variable "env_id" {
  description = "ID of environment"
  type        = string
}

variable "location" {
  description = "The resource group location in Azure"
  default     = "East US 2"
}

variable "resource_group_name" {
  description = "Resource Group Name"
  type        = string
}

variable "automation_account_name" {
  description = "Automation Account Name"
  type        = string
}

variable "runbook_name" {
  description = "Runbook Name"
  type        = string
}

variable "cluster" {
  description = "AKS Cluster"
  type        = string
  validation {
    condition     = length(trimspace(var.cluster)) != 0 && var.cluster != null
    error_message = "'CLUSTER' must be defined as a CI/CD variable."
  }
}

output "cluster" {
  value = var.cluster
}