<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.2.5 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | 3.40.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 3.12.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_automation_account.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/automation_account) | resource |
| [azurerm_automation_job_schedule.start_aks_link](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/automation_job_schedule) | resource |
| [azurerm_automation_job_schedule.stop_aks_link](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/automation_job_schedule) | resource |
| [azurerm_automation_runbook.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/automation_runbook) | resource |
| [azurerm_automation_schedule.start](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/automation_schedule) | resource |
| [azurerm_automation_schedule.stop](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/automation_schedule) | resource |
| [azurerm_resource_group.runbooks](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/resource_group) | resource |
| [azurerm_role_assignment.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/resources/role_assignment) | resource |
| [azurerm_client_config.current](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/data-sources/client_config) | data source |
| [azurerm_subscription.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.40.0/docs/data-sources/subscription) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_base_tags"></a> [base\_tags](#input\_base\_tags) | base tags to apply | <pre>object({<br>    Contact       = string<br>    Created-By    = string<br>    Environment   = string<br>    Multi-Tenancy = string<br>    Owner         = string<br>    Product       = string<br>    Source        = string<br>    Tier          = string<br>  })</pre> | n/a | yes |
| <a name="input_end_date"></a> [end\_date](#input\_end\_date) | null for all resources except which needs to be removed for garbage collection. Formatted as ISO-8601 date | `string` | `""` | no |
| <a name="input_env"></a> [env](#input\_env) | Environment for which resources are created | `string` | n/a | yes |
| <a name="input_env_id"></a> [env\_id](#input\_env\_id) | ID of environment | `string` | n/a | yes |
| <a name="input_group"></a> [group](#input\_group) | Name of the group. pdg or cit | `string` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | The resource group location in Azure | `string` | `"East US 2"` | no |
| <a name="input_location_name"></a> [location\_name](#input\_location\_name) | location Name for resource naming | `string` | n/a | yes |
| <a name="input_log_progress"></a> [log\_progress](#input\_log\_progress) | Log progress | `bool` | `true` | no |
| <a name="input_log_verbose"></a> [log\_verbose](#input\_log\_verbose) | Log versobe | `bool` | `true` | no |
| <a name="input_product_group"></a> [product\_group](#input\_product\_group) | Product group | `string` | `"ifa"` | no |
| <a name="input_product_version"></a> [product\_version](#input\_product\_version) | product version | `string` | `"0.0.0"` | no |
| <a name="input_schedules"></a> [schedules](#input\_schedules) | Runbook schedule | `map(map(string))` | <pre>{<br>  "flytedge": {<br>    "cluster_prefix": "flytedge-",<br>    "start_days": "Monday",<br>    "start_time": "T08:00:00+08:00",<br>    "stop_days": "Saturday",<br>    "stop_time": "T08:00:00+08:00",<br>    "timezone": "Asia/Singapore"<br>  }<br>}</pre> | no |
| <a name="input_timezone"></a> [timezone](#input\_timezone) | Runbook schedule timezone | `string` | `"Asia/Singapore"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_automation_account_name"></a> [automation\_account\_name](#output\_automation\_account\_name) | Automation Account Name |
| <a name="output_resource_group_id"></a> [resource\_group\_id](#output\_resource\_group\_id) | Resource Group ID |
| <a name="output_resource_group_name"></a> [resource\_group\_name](#output\_resource\_group\_name) | Resource Group Name |
| <a name="output_runbook_name"></a> [runbook\_name](#output\_runbook\_name) | Runbook Name |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~>1.2.5 |
| <a name="requirement_azurerm"></a> [azurerm](#requirement\_azurerm) | 3.12.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_azurerm"></a> [azurerm](#provider\_azurerm) | 3.12.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [azurerm_automation_account.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/automation_account) | resource |
| [azurerm_automation_job_schedule.start_aks_link](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/automation_job_schedule) | resource |
| [azurerm_automation_job_schedule.stop_aks_link](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/automation_job_schedule) | resource |
| [azurerm_automation_runbook.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/automation_runbook) | resource |
| [azurerm_automation_schedule.start](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/automation_schedule) | resource |
| [azurerm_automation_schedule.stop](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/automation_schedule) | resource |
| [azurerm_resource_group.runbooks](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/resource_group) | resource |
| [azurerm_role_assignment.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/resources/role_assignment) | resource |
| [azurerm_client_config.current](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/data-sources/client_config) | data source |
| [azurerm_subscription.this](https://registry.terraform.io/providers/hashicorp/azurerm/3.12.0/docs/data-sources/subscription) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_base_tags"></a> [base\_tags](#input\_base\_tags) | base tags to apply | <pre>object({<br>    Contact       = string<br>    Created-By    = string<br>    Environment   = string<br>    Multi-Tenancy = string<br>    Owner         = string<br>    Product       = string<br>    Source        = string<br>    Tier          = string<br>  })</pre> | n/a | yes |
| <a name="input_end_date"></a> [end\_date](#input\_end\_date) | null for all resources except which needs to be removed for garbage collection. Formatted as ISO-8601 date | `string` | `""` | no |
| <a name="input_env"></a> [env](#input\_env) | Environment for which resources are created | `string` | n/a | yes |
| <a name="input_env_id"></a> [env\_id](#input\_env\_id) | ID of environment | `string` | n/a | yes |
| <a name="input_group"></a> [group](#input\_group) | Name of the group. pdg or cit | `string` | n/a | yes |
| <a name="input_location"></a> [location](#input\_location) | The resource group location in Azure | `string` | `"East US 2"` | no |
| <a name="input_location_name"></a> [location\_name](#input\_location\_name) | location Name for resource naming | `string` | n/a | yes |
| <a name="input_log_progress"></a> [log\_progress](#input\_log\_progress) | Log progress | `bool` | `true` | no |
| <a name="input_log_verbose"></a> [log\_verbose](#input\_log\_verbose) | Log versobe | `bool` | `true` | no |
| <a name="input_product_group"></a> [product\_group](#input\_product\_group) | Product group | `string` | `"ifa"` | no |
| <a name="input_product_version"></a> [product\_version](#input\_product\_version) | product version | `string` | `"0.0.0"` | no |
| <a name="input_schedules"></a> [schedules](#input\_schedules) | Runbook schedule | `map(map(string))` | <pre>{<br>  "flytedge": {<br>    "cluster_prefix": "flytedge-",<br>    "start_days": "Monday",<br>    "start_time": "T08:00:00+08:00",<br>    "stop_days": "Saturday",<br>    "stop_time": "T08:00:00+08:00",<br>    "timezone": "Asia/Singapore"<br>  }<br>}</pre> | no |
| <a name="input_timezone"></a> [timezone](#input\_timezone) | Runbook schedule timezone | `string` | `"Asia/Singapore"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_automation_account_name"></a> [automation\_account\_name](#output\_automation\_account\_name) | Automation Account Name |
| <a name="output_resource_group_id"></a> [resource\_group\_id](#output\_resource\_group\_id) | Resource Group ID |
| <a name="output_resource_group_name"></a> [resource\_group\_name](#output\_resource\_group\_name) | Resource Group Name |
| <a name="output_runbook_name"></a> [runbook\_name](#output\_runbook\_name) | Runbook Name |
<!-- END_TF_DOCS -->