locals {
  name_prefix = join("-", [
    "ife",
    "pdg",
    var.env,
    "sre",
    "automation"
  ])
  schedules_list = [for k, v in var.schedules : k]
}

data "azurerm_subscription" "this" {}

resource "azurerm_role_assignment" "this" {
  scope                = data.azurerm_subscription.this.id
  role_definition_name = "Contributor"
  principal_id         = azurerm_automation_account.this.identity[0].principal_id
}

## Create Automation Account
resource "azurerm_automation_account" "this" {
  name                = join("-", [local.name_prefix, "account"])
  location            = azurerm_resource_group.runbooks.location
  resource_group_name = azurerm_resource_group.runbooks.name
  sku_name            = "Basic"
  identity {
    type = "SystemAssigned"
  }
  tags = local.tags
}

## Create Powershell Runbook
resource "azurerm_automation_runbook" "this" {
  name                    = join("-", [local.name_prefix, "runbook"])
  location                = azurerm_resource_group.runbooks.location
  resource_group_name     = azurerm_resource_group.runbooks.name
  automation_account_name = azurerm_automation_account.this.name
  log_verbose             = var.log_verbose
  log_progress            = var.log_progress
  description             = "This Run Book is a to start stop Dev AKS on weekend"
  runbook_type            = "PowerShell"
  content                 = file("src/startstop-aks.ps1")
  tags                    = local.tags
}

## Create Stop Schedules
resource "azurerm_automation_schedule" "stop" {
  for_each                = toset(local.schedules_list)
  name                    = join("", [local.name_prefix, each.key, "stop-schedule"])
  resource_group_name     = azurerm_resource_group.runbooks.name
  automation_account_name = azurerm_automation_account.this.name
  frequency               = "Week"
  interval                = 1
  timezone                = var.schedules[each.key]["timezone"]
  description             = "Stop ${var.schedules[each.key]["cluster_prefix"]} AKS Cluster"
  start_time              = "${substr(timeadd(timestamp(), "24h"), 0, 10)}${var.schedules[each.key]["stop_time"]}"
  week_days               = [var.schedules[each.key]["stop_days"]]
}

## Create Stop Schedule link to Runbook for Common
resource "azurerm_automation_job_schedule" "stop_aks_link" {
  for_each                = toset(local.schedules_list)
  resource_group_name     = azurerm_resource_group.runbooks.name
  automation_account_name = azurerm_automation_account.this.name
  schedule_name           = join("", [local.name_prefix, each.key, "stop-schedule"])
  runbook_name            = azurerm_automation_runbook.this.name
  parameters = {
    action        = "Stop"
    clusterprefix = var.schedules[each.key]["cluster_prefix"]
  }
  depends_on = [
    azurerm_automation_runbook.this,
    azurerm_automation_schedule.stop
  ]
}

## Create Start Schedule for Common
resource "azurerm_automation_schedule" "start" {
  for_each                = toset(local.schedules_list)
  name                    = join("", [local.name_prefix, each.key, "start-schedule"])
  resource_group_name     = azurerm_resource_group.runbooks.name
  automation_account_name = azurerm_automation_account.this.name
  frequency               = "Week"
  interval                = 1
  timezone                = var.schedules[each.key]["timezone"]
  description             = "Start ${var.schedules[each.key]["cluster_prefix"]} AKS Cluster"
  start_time              = "${substr(timeadd(timestamp(), "24h"), 0, 10)}${var.schedules[each.key]["start_time"]}"
  week_days               = [var.schedules[each.key]["start_days"]]
}

## Create Start Schedule link to Runbook for Common
resource "azurerm_automation_job_schedule" "start_aks_link" {
  for_each                = toset(local.schedules_list)
  resource_group_name     = azurerm_resource_group.runbooks.name
  automation_account_name = azurerm_automation_account.this.name
  schedule_name           = join("", [local.name_prefix, each.key, "start-schedule"])
  runbook_name            = azurerm_automation_runbook.this.name
  parameters = {
    action        = "Start"
    clusterprefix = var.schedules[each.key]["cluster_prefix"]
  }
  depends_on = [
    azurerm_automation_runbook.this,
    azurerm_automation_schedule.start
  ]
}

## Outputs
output "automation_account_name" {
  value       = azurerm_automation_account.this.name
  description = "Automation Account Name"
}

output "runbook_name" {
  value       = azurerm_automation_runbook.this.name
  description = "Runbook Name"
}
