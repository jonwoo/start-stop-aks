## Locals
locals {
  resource_name_prefix = join("-", [
    "ife",
    var.group,
    var.env,
    var.location_name,
    var.product_group,
    var.env_id,
    "runbooks"
  ])
}

## Create Resource Group
resource "azurerm_resource_group" "runbooks" {
  name = join("-", [
    local.resource_name_prefix,
    "rg"
  ])

  location = var.location
  tags     = local.tags
}

## Outputs
output "resource_group_name" {
  value       = azurerm_resource_group.runbooks.name
  description = "Resource Group Name"
}

output "resource_group_id" {
  value       = azurerm_resource_group.runbooks.id
  description = "Resource Group ID"
}