## providers
terraform {
  required_version = "~>1.2.5"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.40.0"
    }
  }
}

provider "azurerm" {
  features {}
}

## data
data "azurerm_client_config" "current" {}

## tags
locals {
  tags = merge(
    var.base_tags,
    {
      Subproduct = "01-sre-runbook-start-stop"
      EndDate    = var.end_date
      Version    = var.product_version
    }
  )
}

## variables
variable "base_tags" {
  description = "base tags to apply"
  type = object({
    Contact       = string
    Created-By    = string
    Environment   = string
    Multi-Tenancy = string
    Owner         = string
    Product       = string
    Source        = string
    Tier          = string
  })
}

variable "product_version" {
  description = "product version"
  type        = string
  default     = "0.0.0"
}

variable "env" {
  description = "Environment for which resources are created"
  type        = string
}

variable "end_date" {
  description = "null for all resources except which needs to be removed for garbage collection. Formatted as ISO-8601 date"
  type        = string
  default     = ""
}

variable "group" {
  description = "Name of the group. pdg or cit"
  type        = string
}

variable "location_name" {
  description = "location Name for resource naming"
  type        = string
}

variable "product_group" {
  description = "Product group"
  type        = string
  default     = "ifa"
}

variable "env_id" {
  description = "ID of environment"
  type        = string
}

variable "location" {
  description = "The resource group location in Azure"
  default     = "East US 2"
}

variable "log_verbose" {
  description = "Log versobe"
  type        = bool
  default     = true
}

variable "log_progress" {
  description = "Log progress"
  type        = bool
  default     = true
}

variable "timezone" {
  description = "Runbook schedule timezone"
  type        = string
  default     = "Asia/Singapore"
}

variable "schedules" {
  description = "Runbook schedule"
  default = {
    flytedge = {
      cluster_prefix = "flytedge-"
      timezone       = "Asia/Singapore"
      start_time     = "T08:00:00+08:00"
      stop_time      = "T08:00:00+08:00"
      start_days     = "Monday"
      stop_days      = "Saturday"
    }
  }
  type = map(map(string))
}
