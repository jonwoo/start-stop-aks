Param(
    [Parameter(Mandatory=$True,
    ValueFromPipelineByPropertyName=$false,
    HelpMessage='Specify the operation to be performed on the AKS cluster name (Start/Stop).',
    Position=2)]
    [ValidateSet('Start','Stop')]
    [String]
    $Action,
	[Parameter(Mandatory=$True,
    ValueFromPipelineByPropertyName=$false,
    HelpMessage='Cluster prefix.')]
    [String]
    $ClusterPrefix
)

function ClusterArry($Clusters) {
    $ClusterNamesArr = @()
    Foreach ($Cluster in $Clusters) {
        $ClusterNamesArr += $Cluster
    }

    return $ClusterNamesArr
}

function FindClusters {
    $ResourceType = "Microsoft.ContainerService/ManagedClusters"
    $ClustersToStop = (
        Get-AzResource -ResourceType $ResourceType -Tag @{"autostop"="1"}
        )
    $ClustersToStart = (
        Get-AzResource -ResourceType $ResourceType -Tag @{"autostart"="1"}
        )

    $ClustersToStopArr = ClusterArry($ClustersToStop)
    $ClustersToStartArr = ClusterArry($ClustersToStart)
        
    return $ClustersToStopArr, $ClustersToStartArr
}

function StartStopAksCluster($Cluster) {
    try {
		$ClusterInfo = (Get-AzAksCluster -Name $Cluster.Name -ResourceGroupName $Cluster.ResourceGroupName)
		$ClusterStatus = $ClusterInfo.ProvisioningState
		$ClusterName = $Cluster.Name
		$ClusterRg = $Cluster.ResourceGroupName

    
	    If ($ClusterStatus -eq "Succeeded") {
			try	{
				Switch ($Action) {
					"Start"	{
						If ($ClusterName.StartsWith("$ClusterPrefix"))
						{ 
							Write-Output "This AKS Cluster '$ClusterName' is going to be started now..."
							$Response = (Start-AzAksCluster -Name $ClusterName -ResourceGroupName $ClusterRg)
							Write-Output $Response
						}
						Else
						{
							Write-Output "This AKS Cluster '$ClusterName' is not going to be started"
						}
					}
					
					"Stop" {
						If ($ClusterName.StartsWith("$ClusterPrefix"))
						{
							Write-Output "This AKS Cluster '$ClusterName' is going to be stopped now..."
							$Response = (Stop-AzAksCluster -Name $ClusterName -ResourceGroupName $ClusterRg)
							Write-Output $Response
						}
						Else
						{
							Write-Output "This AKS Cluster '$ClusterName' is not going to be stopped"
						}
					}
					Default {
						Write-Output "Unexpected scenario. The requested operation '$Action' was not matching any of the managed cases."
					}
				}
			}
			catch {
				Write-Error "An error occurred:"
				Write-Error $_
			}		
		} else {
			Write-Output "The AKS Cluster '$ClusterName' is not in '$ClusterStatus' state and '$Action' cannot be performed now!"
		}
    
    }
    catch {
        if (!$AzureContext) {
            $ErrorMessage = "Connect to Azure with system-assigned managed identity was not found"
            throw $ErrorMessage
        }
        else {
            Write-Error -Message $_.Exception
            throw $_.Exception
        }
    }
}

function Main {
	$ClustersToStopArr, $ClustersToStartArr = FindClusters
	if ($Action -eq "Stop") {
		foreach ($Cluster in $ClustersToStopArr) {
			StartStopAksCluster($Cluster)
		}
	} elseif ($Action -eq "Start") {
		foreach ($Cluster in $ClustersToStartArr) {
			StartStopAksCluster($Cluster)
		}
	}
}

# Ensures you do not inherit an AzContext in your runbook
Disable-AzContextAutosave -Scope Process | Out-Null

# Connect to Azure with system-assigned managed identity
$AzureContext = (Connect-AzAccount -Identity).context
Write-Output "$AzureContext"
(Connect-AzAccount -Identity).context

# Setting context to a specific subscription
$AzureContext = Set-AzContext -SubscriptionName $AzureContext.Subscription -DefaultProfile $AzureContext
Write-Output "$AzureContext"
Set-AzContext -SubscriptionName $AzureContext.Subscription -DefaultProfile $AzureContext


# Sign in to your Azure subscription
$sub = Get-AzSubscription -ErrorAction SilentlyContinue
if(-not($sub)) {
	Connect-AzAccount
}

# Main Exec
Main
